SHELL := /bin/bash
develop: disable-env-minipod-test nuke enable-env-minipod-test

enable-env-minipod-test:
	./minipod enable env-minipod-test

disable-env-minipod-test:
	./minipod disable env-minipod-test

lint:
	git ls-files | xargs file | grep Bourne | cut -d ':' -f1 | xargs shellcheck -e SC1091

test: nuke enable-env-minipod-test disable-env-minipod-test

.PHONY: enable-env-minipod-test disable-env-minipod-test lint test nuke encrypt decrypt
