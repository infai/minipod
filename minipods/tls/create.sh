#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" \
    -p="80:80" \
    -p="443:443" \
    --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "TRAEFIK_PROVIDERS_FILE_DIRECTORY=/etc/traefik/dynamic_conf/" \
    --volume "$(realpath ./config/dynamic-config)":/etc/traefik/dynamic_conf \
    docker.io/library/traefik
