#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_db \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "POSTGRES_DB=keycloak" \
    --env "POSTGRES_USER=keycloak" \
    --env "POSTGRES_PASSWORD=$KC_DB_PASSWORD" \
    --volume "$APP_NAME"_db:/var/lib/postgresql/data \
    docker.io/library/postgres:10-alpine

args=(
    --pod "$POD_NAME"
    --name "$APP_NAME"
    --requires "$APP_NAME"_db
    --env "KC_DB=postgres"
    --env "KC_DB_USERNAME=keycloak"
    --env "KC_DB_URL_HOST=localhost"
)
if [[ $IMPORT_REALMS = true ]]; then
    args+=(
        --volume "$(realpath ./config/import/"$MINIPOD_ENV")":/opt/keycloak/data/import
    )
fi
args+=(
    quay.io/keycloak/keycloak:19.0.1
    start
    --import-realm)
podman create \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    "${args[@]}"
