#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_yarrrml-parser \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --label "io.containers.autoupdate=registry" \
    ghcr.io/mat-o-lab/yarrrml-parser:latest

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_rmlmapper-webapi \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --label "io.containers.autoupdate=registry" \
    ghcr.io/mat-o-lab/rmlmapper-webapi:latest

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --label "io.containers.autoupdate=registry" \
    ghcr.io/mat-o-lab/rdfconverter:latest gunicorn -b 0.0.0.0:8080 wsgi:app --workers=3
