#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --volume "$APP_NAME"_logs:/var/log/onlyoffice \
    --volume "$APP_NAME"_data:/var/www/onlyoffice/Data \
    --volume "$APP_NAME"_fonts:/usr/share/fonts/truetype/custom \
    --volume "$APP_NAME"_lib:/var/lib/onlyoffice \
    --volume "$APP_NAME"_rabbitmq:/var/lib/rabbitmq \
    --volume "$APP_NAME"_redis:/var/lib/redis \
    --volume "$APP_NAME"_db:/var/lib/postgresql \
    --memory 6g \
    --memory-swap 6500m \
    --label "io.containers.autoupdate=registry" \
    docker.io/onlyoffice/documentserver:7.1 
