#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_api \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "ALLOWED_ORIGINS=*" \
    --env "JAVA_OPTS=-Xmx512m -Xms128m -Dcom.sun.security.enableAIAcaIssuers=true" \
    docker.io/rhizomik/rhizomer-api
