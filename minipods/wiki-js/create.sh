#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_db \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "POSTGRES_DB=wiki" \
    --env "POSTGRES_USER=wikijs" \
    --volume "$APP_NAME"_db:/var/lib/postgresql/data \
    docker.io/library/postgres:11-alpine

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_app \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "DB_TYPE=postgres" \
    --env "DB_HOST=localhost" \
    --env "DB_PORT=5432" \
    --env "DB_USER=wikijs" \
    --env "DB_NAME=wiki" \
    --env "PORT=8080" \
    --volume "$(realpath "./config/config.yml")":/wiki/config.yml \
    ghcr.io/requarks/wiki:2
