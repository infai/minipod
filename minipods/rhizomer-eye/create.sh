#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_eye \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "API_URL=http://rhizomer-api.${MINIPOD_HOST}:$MINIPOD_PORT_HTTP" \
    docker.io/rhizomik/rhizomer-eye
