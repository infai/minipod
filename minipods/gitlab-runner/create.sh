#!/bin/bash

systemctl --user --now enable podman.socket

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    -v /var/run/user/"$(id -u)"/podman/podman.sock:/var/run/docker.sock \
    -v "$APP_NAME"_config:/etc/gitlab-runner \
    -v "$APP_NAME"_home:/home/gitlab-runner \
    gitlab/gitlab-runner:latest

podman run --rm \
    -v "$APP_NAME"_config:/etc/gitlab-runner \
    gitlab/gitlab-runner unregister --all-runners

podman run --rm \
    -v "$APP_NAME"_config:/etc/gitlab-runner \
    gitlab/gitlab-runner register \
    --non-interactive \
    --executor="docker" \
    --docker-privileged="true" \
    --docker-image "$RUNNER_IMAGE" \
    --docker-tlsverify="false" \
    --docker-host="unix:///var/run/docker.sock" \
    --docker-volumes="$APP_NAME"_rootless_containers:/home/podman/.local/share/containers:rw \
    --docker-volumes="$APP_NAME"_root_containers:/var/lib/containers:rw \
    --url="$RUNNER_URL" \
    --registration-token="$RUNNER_TOKEN" \
    --description="$RUNNER_DESCRIPTION" \
    --tag-list="$RUNNER_TAGS" \
    --run-untagged="true" \
    --locked="false" \
    --access-level="not_protected"
