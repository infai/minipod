FROM docker.io/ubuntu:22.04 as production

ENV DATAPUSHER_PLUS_VERSION=0.15.0 \
  DATEUTIL__VERSION=2.8.2 \
  DEBUGPY__VERSION=1.6.6 \
  UWSGI__VERSION=2.0.21 \
  TZDATA__VERSION=2023.3 \
  QSV_VERSION=0.108.0

ENV VENV=/usr/lib/datapusher-plus
ENV CFG_DIR=/etc/ckan/datapusher-plus

# Instal apt-utils
RUN apt-get update && apt-get install --no-install-recommends -y \
  apt-utils \
  python3-venv \
  python3-dev \
  python3 \
  build-essential \
  locales \
  libxslt1-dev \
  libxml2-dev \
  libffi-dev \
  curl \
  unzip \
  postgresql-client \
  python3-pip \
  git\
  libpq-dev\
  file \
  && apt-get -qq clean \
  && rm -rf /var/lib/apt/lists/*

# Set Locale
RUN locale-gen de_DE.UTF-8 en_US.UTF-8
ENV LANG=de_DE.UTF-8
ENV LANGUAGE=de_DE.UTF-8
ENV LC_ALL=de_DE.UTF-8

# Install qsv;
RUN curl -sSL https://github.com/jqnatividad/qsv/releases/download/${QSV_VERSION}/qsv-${QSV_VERSION}-x86_64-unknown-linux-gnu.zip -o /tmp/qsv.zip
RUN unzip /tmp/qsv.zip -d /tmp
RUN rm /tmp/qsv.zip
RUN mv /tmp/qsv* /usr/local/bin/
RUN /usr/local/bin/qsvdp --update

# Create virtual environment
RUN python3 -m venv ${VENV}
# Activate virtual environment 'ckan' for all subsequent RUN commands
ENV PATH="$VENV/bin:$PATH"
RUN pip install --no-cache-dir \
  uwsgi==${UWSGI__VERSION} \
  tzdata==${TZDATA__VERSION} \
  python-dateutil==${DATEUTIL__VERSION}

# Install datapusher-plus from source repo
RUN pip install --no-cache-dir --quiet \
  -e git+https://github.com/datHere/datapusher-plus.git@${DATAPUSHER_PLUS_VERSION}#egg=datapusher-plus

WORKDIR ${VENV}

COPY init-datapusher-plus.sh .
COPY uwsgi.ini  ${CFG_DIR}/uwsgi.ini
COPY uwsgi-debug.ini  ${CFG_DIR}/uwsgi-debug.ini

ENTRYPOINT [ "./init-datapusher-plus.sh" ]

CMD ["bin/uwsgi", "--ini", "/etc/ckan/datapusher-plus/uwsgi.ini"]


FROM production as development
RUN pip install --no-cache-dir debugpy=="$DEBUGPY__VERSION"
