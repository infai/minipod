#!/bin/bash

while ! psql -l "$WRITE_ENGINE_URL" >/dev/null; do
    echo "Waiting for database"
    sleep 5
done

exec "$@"
