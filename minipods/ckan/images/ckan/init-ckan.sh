#!/bin/bash
set -e

log() {
    printf 'CKAN init script: %s\n' "$@"
}

# For each bind-mounted subdirectory in "$APP__DIR/src",
# install the extension in development mode.
install_development_extensions() {
    for dir in "$APP__DIR"/src/*; do
        if grep -qE "^[^ ]+ $(realpath "$dir") " /proc/mounts; then
            cd "$dir"
            echo "Installing extension in $dir"
            pip install -e .

            if [ -f "$dir/dev-requirements.txt" ]; then
                echo "Installing dev-requirements for $dir"
                pip install -r "$dir/dev-requirements.txt"
            fi
        fi
    done
}

generate_config() {
    log "Generating config"
    ckan generate config "$CKAN_INI"
    if [ -f "/etc/merge.ini" ]; then
        log "Merging config"
        ckan config-tool -f /etc/merge.ini "$CKAN_INI"
    fi
}

create_admin_user() {
    log "Creating admin user"
    user_query=$(ckan user show "$CKAN_SYSADMIN_NAME")
    if [[ $user_query == "User: None" ]]; then
        ckan user add "$CKAN_SYSADMIN_NAME" \
            password="$CKAN_SYSADMIN_PASSWORD" \
            email="$CKAN_SYSADMIN_EMAIL"
        ckan sysadmin add "$CKAN_SYSADMIN_NAME"
    fi
}

add_datapusher_token() {
    log "Add datapusher api token to config"
    ckan config-tool "$CKAN_INI" "ckan.datapusher.api_token=$(
        ckan user token add "$CKAN_SYSADMIN_NAME" datapusher |
            tail -n 1 |
            tr -d '\t'
    )"
}

init_db() {
    log "Init db"
    ckan db init
    log "Init datastore"
    ckan datastore set-permissions | psql -U postgres -d ckan -h "$CKAN__DATABASE_HOST"
    log "Init harvester db"
    ckan harvester initdb
    log "Init pages"
    ckan pages initdb
}

sed -i "s/config\[u\"ckan\.plugins\"\]\.split()/config[u\"ckan.plugins\"]/" /srv/app/src/ckan/ckan/cli/views.py

if [ ! -v "$CKAN_SITE_URL" ]; then
    while ! pg_isready -U postgres -h "$CKAN__DATABASE_HOST"; do
        log "Waiting for database"
        sleep 5
    done
    while ! curl -s -o /dev/null -w "%{http_code}" "${CKAN_SOLR_URL}/admin/ping?wt=json" | grep -q '200'; do
        log "Waiting for Solr"
        sleep 5
    done
fi

install_development_extensions
# When the te root process is `uwsgi` 
# or the container was started with `ckan run`, it is not a worker process
if [[ "$1" != "uwsgi" && ! "$3" =~ "ckan run" ]]; then
    while ! curl -s -o /dev/null -w "%{http_code}" "ckan:8080/api/3/action/status_show" | grep -q '200'; do
        log "Waiting for CKAN"
        sleep 5
    done
else
    log "Init CKAN"
    [ -f "$CKAN_INI" ] && is_first_start=false || is_first_start=true
    generate_config
    if "$is_first_start"; then
        log "First startup, initialising databases"
        init_db
    fi
    create_admin_user
    add_datapusher_token
fi
exec "$@"
