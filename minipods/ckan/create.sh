#!/bin/bash

# Only add the debug port, when $DEBUG_PORT is defined
args=("--replace=true" "--name=$POD_NAME" "--network=$MINIPOD_PODMAN_NETWORK")
podman pod create "${args[@]}"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_postgresql \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "PGDATA=/var/lib/postgresql/data/db" \
    --volume "$APP_NAME"_postgresql_data:/var/lib/postgresql/data \
    postgresql:minipod

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_solr \
    --volume "$APP_NAME"_solr_data:/var/solr \
    solr:minipod

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_redis \
    --volume "$APP_NAME"_redis_data:/data \
    docker.io/library/redis:alpine

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_datapusher_plus \
    --requires "$APP_NAME"_postgresql \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "SQLALCHEMY_DATABASE_URI=postgresql://datapusher_jobs:$POSTGRES_PASSWORD@$CKAN__DATABASE_HOST:$CKAN__DATABASE_PORT/datapusher_jobs" \
    --env "WRITE_ENGINE_URL=postgresql://ckan:$POSTGRES_PASSWORD@$CKAN__DATABASE_HOST:$CKAN__DATABASE_PORT/datastore" \
    datapusher_plus_"$IMAGE_STAGE":minipod

function create_ckan_container() {
    local name="$1"
    local cmd="$2"

    # Create a temporary file and store the envsubst output
    temp_env_file=$(mktemp)
    envsubst <./config/"$MINIPOD_ENV" >"$temp_env_file"

    ckan_create_command=(
        podman create --pod "$POD_NAME"
        --name "$APP_NAME""$name"
        --env-file "$temp_env_file"
        --requires "$APP_NAME"_postgresql
        --requires "$APP_NAME"_redis
        --requires "$APP_NAME"_solr
        --env "CKAN_SQLALCHEMY_URL=postgresql://ckan:$POSTGRES_PASSWORD@$CKAN__DATABASE_HOST:$CKAN__DATABASE_PORT/ckan"
        --env "CKAN_DATASTORE_WRITE_URL=postgresql://ckan:$POSTGRES_PASSWORD@$CKAN__DATABASE_HOST:$CKAN__DATABASE_PORT/datastore"
        --env "CKAN_DATASTORE_READ_URL=postgresql://datastore:$POSTGRES_DATASTORE_PASSWORD@$CKAN__DATABASE_HOST:$CKAN__DATABASE_PORT/datastore"
        --env "CKAN_REDIS_URL=redis://localhost:6379/1"
        --env "CKAN_SOLR_URL=http://localhost:8983/solr/ckan"
        --volume "$APP_NAME"_storage:/var/lib/ckan
        --volume "$(realpath "$CKAN__INI_FILE")":/etc/merge.ini
        --volume "$APP_NAME"_config:/etc/ckan
        ckan_"$IMAGE_STAGE":minipod
    )
    if [ "$cmd" != "" ]; then
        ckan_create_command+=(bash -c "$cmd")
    fi
    "${ckan_create_command[@]}"
}

create_ckan_container "" ""
create_ckan_container _harvest_gather "ckan harvester gather-consumer"
create_ckan_container _harvest_fetch "ckan harvester fetch-consumer"
create_ckan_container _harvest_run "while true; do sleep 900; ckan harvester run; done"
