#!/bin/bash

podman pod create --replace=true --name "$POD_NAME" --network="$MINIPOD_PODMAN_NETWORK"

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_mariadb \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env "MYSQL_DATABASE=nextcloud" \
    --env "MYSQL_USER=nextcloud" \
    --volume "$APP_NAME"_db:/var/lib/mysql \
    --health-cmd "mysqladmin ping --silent" \
    --health-interval 10s \
    --health-retries 3 \
    --health-timeout 5s \
    --health-start-period 10s \
    --label "io.containers.autoupdate=registry" \
    docker.io/library/mariadb:10.5

podman create --pod "$POD_NAME" \
    --name "$APP_NAME"_clamav \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --label "io.containers.autoupdate=registry" \
    docker.io/clamav/clamav:stable 

podman create --pod "$POD_NAME" \
    --name "$APP_NAME" \
    --requires "$APP_NAME"_mariadb \
    --env-file <(envsubst <./config/"$MINIPOD_ENV") \
    --env DOMAIN="$APP_NAME.$MINIPOD_HOST" \
    --env "DB_TYPE=mysql" \
    --env "DB_NAME=nextcloud" \
    --env "DB_USER=nextcloud" \
    --env "DB_HOST=127.0.0.1" \
    --volume "$APP_NAME"_data:/data \
    --volume "$APP_NAME"_config:/nextcloud/config \
    --volume "$APP_NAME"_apps:/nextcloud/apps2 \
    --volume "$APP_NAME"_themes:/nextcloud/themes \
    --label "io.containers.autoupdate=registry" \
    ghcr.io/hoellen/nextcloud:24 \
    sh -c "sed -i 's/8888/8080/' /etc/nginx/conf.d/default.conf && run.sh"
